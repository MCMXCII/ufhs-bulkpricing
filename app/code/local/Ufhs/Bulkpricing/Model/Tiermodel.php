<?php

/**
 * Bulk Pricing Model
 *
 * @version     $Id$
 * @package     Ufhs_Bulkpricing
 * @author      Dominic Sutton <dominic.sutton@theunderfloorheatingstore.com>
 *
 */
class Ufhs_Bulkpricing_Model_Tiermodel extends Mage_Core_Model_Abstract
{
    /**
     * Validate the input parameters
     *
     * Ensures both prod and qty are set and valid numeric values.
     *
     * @param  Array    $params     The GET vars
     * @return Boolean  Are they valid
     */
    public function validateParams(Array $params)
    {
        if(!isset($params['prod']))
        {
            throw new Exception(__('Product ID has not been set.'));
        }
        if(!isset($params['qty']))
        {
            throw new Exception(__('Quantity has not been set.'));
        }
        if(!preg_match('/^\d+$/',$params['prod']))
        {
            throw new Exception(__('Product ID contains something other than numeric characters.'));
        }
        if(!preg_match('/^\d+$/',$params['qty']))
        {
            throw new Exception(__('Quantity contains something other than numeric characters.'));
        }
        return $params;
    }
    /**
     * Return final tier price
     *
     * Using the product ID and quantity, lookup and return the correct tier
     * pricing for that product.
     *
     * @param  String   $prodId     The product ID
     * @param  String   $qty        The quantity
     * @return Float    The final tier price
     */

    public function getFinalTierPrice($prodId, $qty)
    {
        $product = Mage::getModel('catalog/product')->load($prodId);
        $customerGroupId = Mage::getModel('customer/session')->getCustomerGroupId();
        if(!$product->getSku())
        {
            throw new Exception(__('Unable to load product.'));
        }
        $tiers[0] = floatval($product->price);
        foreach ($product->tier_price as $tier)
        {
            if(intval($tier['price_qty']) <= intval($qty))
            {
                if($customerGroupId == $tier['cust_group'] || (!$this->_isTraderId($customerGroupId) && $tier['all_groups'] == 1))
                {
                    $tiers[intval($tier['price_qty'])] = floatval($tier['price']);
                }
            }
        }
        $finalPrice = $tiers[max(array_keys($tiers))];

        $baseCurrencyCode = Mage::app()->getStore()->getBaseCurrencyCode();
        $currentCurrencyCode = Mage::app()->getStore()->getCurrentCurrencyCode();
        if ($baseCurrencyCode != $currentCurrencyCode) {
        	$finalPrice = Mage::helper('directory')->currencyConvert($finalPrice, $baseCurrencyCode, $currentCurrencyCode);
        }
        return $finalPrice;
    }

    /**
     * Is Trader
     * ---------
     * Check to see whether a passd ID relates to the ID of a trader account
     *
     * @param  Int $id The request ID
     * @return Boolean Whether the requested ID is a trader ID
     */
    private function _isTraderId($id)
    {
        $collection = Mage::getModel('customer/group')->getCollection()
        ->addFieldToFilter('customer_group_code',['Bronze','Silver','Gold'])->getColumnValues('customer_group_id');
        return in_array($id,$collection);
    }
}